package im.ene.lab.neila;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * Example shell activity which simply broadcasts to our receiver and exits.
 */
public class MyStubBroadcastActivity extends Activity {
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    Intent i = new Intent();
    i.setAction("im.ene.lab.neila.SHOW_NOTIFICATION");
    i.putExtra(MyPostNotificationReceiver.CONTENT_KEY, getString(R.string.title));
    sendBroadcast(i);
    finish();
  }
}
