package im.ene.lab.neila.api;

import im.ene.lab.neila.model.Login;
import im.ene.lab.neila.model.Result;
import retrofit.Call;

/**
 * Created by eneim on 11/29/15.
 */
public final class ApiClient {

  public static Call<Result<Login>> login(String user, String password) {
    return Api.user.login(user, password);
  }
}
