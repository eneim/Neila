package im.ene.lab.neila.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import im.ene.lab.neila.model.Login;
import im.ene.lab.neila.model.Result;
import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

import java.io.IOException;

/**
 * Created by eneim on 11/29/15.
 */
final class Api {

  private static final String BASE_URL = "https://api.reddit.com";

  private static final OkHttpClient client = new OkHttpClient();

  private static final Gson gson = new GsonBuilder()
      .excludeFieldsWithoutExposeAnnotation()
      .create();

  private static final Retrofit retrofit = new Retrofit.Builder()
      .client(client)
      .addConverterFactory(GsonConverterFactory.create(gson))
      .baseUrl(BASE_URL).build();

  static final User user = retrofit.create(User.class);

  static {
    client.networkInterceptors().add(new Interceptor() {
      @Override public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        HttpUrl url = request.httpUrl().newBuilder()
            .addQueryParameter("api_type", "json").build();
        return chain.proceed(request.newBuilder().url(url).build());
      }
    });
  }

  interface User {

    @FormUrlEncoded
    @POST("/api/login.json") Call<Result<Login>> login(@Field("user") String user,
                                                          @Field("passwd") String password);
  }

}
