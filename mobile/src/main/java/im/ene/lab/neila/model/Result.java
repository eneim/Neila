package im.ene.lab.neila.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by eneim on 11/29/15.
 */
public class Result<M> {

  @SerializedName("json")
  @Expose
  private M json;

  public M getJson() {
    return json;
  }

  public void setJson(M json) {
    this.json = json;
  }
}
