package im.ene.lab.neila.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.support.annotation.Nullable;

import java.util.ArrayList;


public class Login {

  @SerializedName("errors")
  @Expose
  private ArrayList<ArrayList<String>> errors = new ArrayList<>();

  @Nullable
  @SerializedName("data")
  @Expose
  private Data data;

  public ArrayList<ArrayList<String>> getErrors() {
    return errors;
  }

  public void setErrors(ArrayList<ArrayList<String>> errors) {
    this.errors = errors;
  }

  @Nullable
  public Data getData() {
    return data;
  }

  public void setData(@Nullable Data data) {
    this.data = data;
  }

  public static class Data {

    @SerializedName("need_https")
    @Expose
    private Boolean needHttps;
    @SerializedName("modhash")
    @Expose
    private String modhash;
    @SerializedName("cookie")
    @Expose
    private String cookie;

    public Boolean getNeedHttps() {
      return needHttps;
    }

    public void setNeedHttps(Boolean needHttps) {
      this.needHttps = needHttps;
    }

    public String getModhash() {
      return modhash;
    }

    public void setModhash(String modhash) {
      this.modhash = modhash;
    }

    public String getCookie() {
      return cookie;
    }

    public void setCookie(String cookie) {
      this.cookie = cookie;
    }

    @Override public String toString() {
      return "Data{" +
          "needHttps=" + needHttps +
          ", modhash='" + modhash + '\'' +
          ", cookie='" + cookie + '\'' +
          '}';
    }
  }

  @Override public String toString() {
    return "Login{" +
        "errors=" + errors +
        ", data=" + data +
        '}';
  }
}
